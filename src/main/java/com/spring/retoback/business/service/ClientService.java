package com.spring.retoback.business.service;

import com.spring.retoback.business.model.api.ResponseKpi;
import com.spring.retoback.business.model.entity.Client;

import java.util.List;

public interface ClientService {

    public Client createClient(Client client);

    public ResponseKpi getClientKpi();

    public List<Client> getClients();
}
