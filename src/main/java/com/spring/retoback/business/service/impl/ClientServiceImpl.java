package com.spring.retoback.business.service.impl;

import com.spring.retoback.business.dao.repository.ClientRepository;
import com.spring.retoback.business.model.api.ResponseKpi;
import com.spring.retoback.business.model.entity.Client;
import com.spring.retoback.business.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public Client createClient(Client client) {
        return clientRepository.save(client);
    }

    @Override
    public ResponseKpi getClientKpi() {
        ResponseKpi responseKpi = new ResponseKpi();
        List<Client> clients = clientRepository.findAll();

        responseKpi.setAverageAge(average(clients));
        responseKpi.setStandarDeviation(deviation(clients,responseKpi.getAverageAge()));


        return responseKpi;
    }

    @Override
    public List<Client> getClients() {
        List<Client> clients = clientRepository.findAll();
        for(Client client: clients){
            client.mortality();
        }
        return clients;
    }

    private Integer average(List<Client> clients){
        Integer addAge = 0;
        for(Client client: clients){
            addAge+=client.getAge();
        }
        return addAge/clients.size();
    }

    private Double deviation(List<Client> clients, Integer average){
        int devia = 0;
        for(Client client: clients){
            devia+=Math.pow(Long.valueOf(client.getAge()-average), Long.valueOf(2)) ;
        }
        return Math.sqrt(devia);
    }
}
