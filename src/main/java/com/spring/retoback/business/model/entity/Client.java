package com.spring.retoback.business.model.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@Entity
@Table(name = "client")
@AllArgsConstructor
@NoArgsConstructor
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(
            name = "id",
            dataType = "Integer",
            example = "1",
            position = 1
    )
    private Integer id;

    @Column(name = "name")
    @ApiModelProperty(
            name = "name",
            dataType = "String",
            example = "Ricardo",
            required = true,
            position = 2
    )
    private String name;

    @Column(name = "last_name")
    @ApiModelProperty(
            name = "lastName",
            dataType = "String",
            example = "Montez",
            required = true,
            position = 3
    )
    private String lastName;

    @Column(name = "age")
    @ApiModelProperty(
            name = "age",
            dataType = "Integer",
            example = "20",
            required = true,
            position = 4
    )
    private Integer age;

    @JsonFormat(pattern = "dd-MM-yyyy")
    @Column(name = "birthdate")
    @ApiModelProperty(
            name = "birthdate",
            dataType = "LocalDate",
            example = "10-04-2010",
            required = true,
            position = 5
    )
    private LocalDate birthdate;

    @JsonFormat(pattern = "dd-MM-yyyy")
    @Transient
    @ApiModelProperty(
            name = "probableMortality",
            dataType = "LocalDate",
            example = "20-10-2030",
            position = 6
    )
    public LocalDate probableMortality;

    public void mortality(){
        if(age != null){
            probableMortality = birthdate.plusYears(age);
        }
    }
}
