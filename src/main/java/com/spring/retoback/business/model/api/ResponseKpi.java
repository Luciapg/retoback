package com.spring.retoback.business.model.api;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseKpi {

    @ApiModelProperty(
            name = "averageAge",
            dataType = "Integer",
            example = "23",
            position = 1
    )
    private Integer averageAge;

    @ApiModelProperty(
            name = "standarDeviation",
            dataType = "Double",
            example = "10.44030650891055",
            position = 2
    )
    private Double standarDeviation;
}
