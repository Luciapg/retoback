package com.spring.retoback.controller;

import com.spring.retoback.business.model.api.ResponseKpi;
import com.spring.retoback.business.model.entity.Client;
import com.spring.retoback.business.service.ClientService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/rest")
@Api(value = "Client Resource", description = "Rest Endpoints")
public class ClientController {

    @Autowired
    private ClientService clientService;

    @ApiOperation(value = "Create clients")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Successful!" ),
                    @ApiResponse(code = 500, message = "Fatal Error!")
            }
    )
    @PostMapping(value = "/creacliente", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Client createClient(@RequestBody Client client){
        return clientService.createClient(client);
    }

    @ApiOperation(value = "Get Kpi clients")
    @GetMapping(value = "/kpideclientes")
    public ResponseKpi getKpiClient(){
        return clientService.getClientKpi();
    }

    @ApiOperation(value = "Get clients")
    @GetMapping(value = "/listclientes")
    public List<Client> getClients(){
        return clientService.getClients();
    }

}
